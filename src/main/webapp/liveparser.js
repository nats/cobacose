/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */

var jwcdg = jwcdg || {};
jwcdg.liveparser = (function() {
	var lastSentence = '';

	$(document).ready(function() {
		setTimeout(sendSentence, 5000);
	});

	function sendSentence() {
		//Check input and send request if input changed.
		var currentSentence = $('#input').val();
		if (currentSentence !== lastSentence) {
			$.ajax({
				url : 'LPServlet',
				type : 'GET',
				data : {
					sentence : currentSentence
				},
				async : true,
			}).done(function(data) {
				
				$('#result').html('<pre>'+data+'</pre>');
			});
		}
		lastSentence = currentSentence;
		setTimeout(sendSentence, 5000);
	}

}());