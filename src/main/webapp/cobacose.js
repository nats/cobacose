/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */

var jwcdg = jwcdg || {};
jwcdg.cobacose = (function($) {

	"use strict";
	// Variables
	var rpp = 10, STEPSIZE = 1000, MINVAL = 1, maxval, examples = [
			"X/ & Y\\ & X\\Y & Y^from > X@from ;",
			"X.label = SUBJ ;",
			"X@word = nicht | X@word = nichts | X@word = niemals | X@word = nirgends | X@word = nie | X@word = kaum | X@word = keinesfalls | X@word = keineswegs | X@word = keinerlei | X@base = kein |X@base = niemand ;" ];

	$(document).ready(function() {

		// Request corpussize
		$.ajax({
			url : "CSServlet",
			type : 'POST',
			async : true,
			success : function(data) {
				maxval = parseInt(data, 10);
				$('#corpussize').text(maxval);
				// initialize custom ui widgets
				$('#from').spinner({
					step : STEPSIZE,
					// keep "from" within the limits [1, from]
					change : fromspin
				});
				$('#from').val(1);

				$('#to').spinner({
					step : STEPSIZE,
					// keep "to" within the limits [to,maxval]
					change : tospin
				});
				$('#to').val(maxval);

				$('#constraint').autocomplete({
					source : examples
				});
			}
		});

		$('#stop').attr("disabled", true);
		$('#stop').hide();

		// Add event handlers to UI elements
		$('#stop').click(stoprequest);
		$('#search').click(submitrequest);
		$('#setA').click(function() {
			setsearchrange(1, 102000);
		});
		$('#setB').click(function() {
			setsearchrange(102001, 202000);
		});
		$('#setC').click(function() {
			setsearchrange(202001, 252000);
		});
		
		$('#constraint').keydown(function(event) {
			keyinput(event);
		});

		$('iframe').iFrameResize();

	});

	function setsearchrange(min, max) {
		$('#from').val(min);
		$('#to').val(max);
	}	

	function submitrequest() {
		var from, to, constraint, url;

		$('#errormsg').text('');
		$('#stop').attr("disabled", false);
		$('#stop').show();

		// read values from the UI
		from = parseInt(document.getElementById("from").value, 10);
		to = parseInt(document.getElementById("to").value, 10);
		constraint = $('#constraint').val();
		if ($.inArray(constraint, examples) === -1) {
			examples.push(constraint);
		}

		// reset progress shown in the UI
		updateProgress(0);
		
		url = 'results.html?rpp=' + rpp + '&from=' + from + '&to=' + to + '&constraint='
				+ encodeURIComponent(constraint);		
		$('#resultframe').attr('src', url);

	}

	function stoprequest() {
		$('#stop').attr("disabled", true);
		window.frames[0].jwcdg.results.stopSearch();
	}

	/*
	 * Function to keep the "from" input within the bounds [MINVAL,to]
	 */
	function fromspin(event) {
		var target = $(event.target), targetval = parseInt(target.val(), 10), otherval = parseInt($('#to').val(), 10);

		if (!isNaN(targetval)) {
			
			if (targetval < MINVAL) {
				target.val(MINVAL);
			}
			if (targetval > otherval) {
				target.val(otherval);
			}
		} else {
			target.val(MINVAL);
		}
	}

	/*
	 * Function to keep the "to" input within the bounds [from, MAXVAL]
	 */
	function tospin(event) {
		var target = $(event.target), targetval = parseInt(target.val(), 10), otherval = parseInt($('#from').val(), 10);

		if (!isNaN(targetval)) {
			
			if (targetval < otherval) {
				target.val(otherval);
			}
			if (targetval > maxval) {
				target.val(maxval);
			}
		} else {
			target.val(maxval);
		}
	}

	function keyinput(event) {
		// keycode 13 is ENTER
		if (event.keyCode === 13) {
			submitrequest();
		}
	}

	// function to escape special characters which may appear both in regEX and
	// constraints
	function regexEscape(str) {
		return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	}

	function updateProgress(progress) {

		if (progress >= 100) {
			progress = 100;
			$('#stop').attr("disabled", true);
			$("#progress").html(
					'<h1 style="display:inline">'+ progress.toFixed(1) + "% </h1>");
		}
		else
		{
		$("#progress").html(
				'<h1 style="display:inline">Searching ... '
						+ progress.toFixed(1) + "% </h1>");
		}
	}

	function handleError(error) {
		var original, token, index, input;
		// This function will only be called, if there is an error in the
		// constraint
		// stop the request and mark the errornous part of the
		// constraint

		original = $('#constraint').val();
		// the error messages are structured like this:
		// <Possible Reason for Error>-<Token>
		token = error["message"].split("-");
		index = original.search(regexEscape(token[1]));
		input = document.getElementById('constraint');
		input.setSelectionRange(index, index + token[1].length);
		input.focus();
		$('#errormsg').text(token[0]);
	}

	function rememberRPP(size)
	{
		rpp = size;
	}
	
	// public API
	return {
		handleError : handleError,
		updateProgress : updateProgress,
		rememberRPP : rememberRPP
	};

}(jQuery));