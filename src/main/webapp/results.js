/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */

var jwcdg = jwcdg || {};
jwcdg.results = (function($) {

	"use strict";
	// Variables
	var fileprefix = '', filesuffix = '', prog = 0, STEPSIZE = 1000, PAGEJUMP = 10, diff = 1, finished = true, entriesPerPage, currentPage = 0, maximumPage = 0, resultPages = [], matchedfiles = [];

	$(document).ready(function() {
		var query, vars, from = 0, to = 0, constraint = '', i = 0, index;

		$('#resultsperpage').change(function() {
			updateEntriesPerPage($('#resultsperpage').val());
		});
		//entriesPerPage = parseInt($('#resultsperpage').val(), 10);

		// read parameters
		query = window.location.search.substring(1);		
		
		
		vars = query.split('&');		
		for (i = 0; i < vars.length;i += 1)
		{
			index = vars[i].indexOf('=');
			//rpp=
			if(index === 3)
			{				
				entriesPerPage = parseInt(vars[i].substring(4), 10);
				$('#resultsperpage').val(entriesPerPage);
			}
			//from=
			if(index === 4)
			{				
				from = parseInt(vars[i].substring(5), 10);								
			}
			//to=
			if(index === 2)
			{				
				to = parseInt(vars[i].substring(3), 10);							
			}
			//constraint=
			if(index === 10)
			{				
				constraint = decodeURIComponent(vars[i].substring(11));								
			}
		}

		diff = to - from;
		finished = false;
		if (constraint !== '') {
			longpolling(from, from + STEPSIZE - 1, constraint, to);
		}
	});

	// Set the current resultpage to display
	function setPage(pagenumber) {

		if (pagenumber !== currentPage) {
			currentPage = pagenumber;
			// UPdate UI
			clearUI();
			buildResultNavigation();
			displayResults();
		}
	}
	
	// constructor for request objects
	function Request(from, to, constraint) {
		return {
			from : from,
			to : to,
			constraint : constraint
		};
	}
	
	function longpolling(from, to, constraint, limit) {
		
		// send request to asynchronous Servlet
		$.ajax({
			url : 'CSServlet',
			type : 'GET',
			data : new Request(from, to, constraint),
			async : true
		}).done(function(data) {			
			if (!finished) {// Adjust Request content
				from += STEPSIZE;
				to += STEPSIZE;
				if (to > limit) {
					to = limit;
				}
				if (from > limit) {
					finished = true;
					processData(data);
				} else {
					// Process Data and send new Request
					processData(data);
					longpolling(from, to, constraint, limit);
				}
			}
		});

	}

	
	function clearUI() {		
		$('#results').empty();
	}

	function clearData() {
		clearUI();
		resultPages = [];
		maximumPage = 0;
		currentPage = 0;
	}

	function loadImage(div) {
		var image, data;
		// read the image-data field
		image = div.children('object');
		data = image[0].dataset.image;

		// load image by setting the data to image-data
		if (data !== '') {
			image[0].dataset.image = '';
			image[0].data = data;
		}
	}

	function updateEntriesPerPage(value) {
		// Adjust values
		entriesPerPage = parseInt(value, 10);
		if(self!=top)
		{
			parent.jwcdg.cobacose.rememberRPP(entriesPerPage);
		}
		maximumPage = Math.floor(resultPages.length / entriesPerPage);

		// The selected Page might be outside of [0,maximumPage]
		if (currentPage > maximumPage) {
			currentPage = 0;
		}

		// refresh the UI
		clearUI();
		buildResultNavigation();
		displayResults();
	}

	function displayResults() {
		var results, fragment, start, i, end, div, downloadmatches;

		// display results of the currently active page
		// results are added to a documentfragment to reduce the number of
		// reflows
		results = $('#results');
		fragment = document.createDocumentFragment();
		start = currentPage * entriesPerPage;
		for (i = start, end = start + entriesPerPage; i < end
				&& (i < resultPages.length); i++) {

			fragment.appendChild(resultPages[i][0]);
			fragment.appendChild(resultPages[i][1]);
		}
		// fragment.appendChild(wrapper);
		results.append(fragment);
		// console.log('appending fragment: ' + fragment);
		// Add onclick functionality
		$('#results h3').click(function() {
			div = $(this).next();
			$(".tab > div").each(function() {
				loadImage($(this));
			});

			div.toggle();
			return false;
		}).next().hide();

		$('.tab').each(function() {
			$(this).tabs();
		});

		downloadmatches = '<a href="javascript:void(0)" onclick="jwcdg.results.downloadMatchedFiles()" download="matchedfiles.zip">Download Matched Files</a>';
		results.append(downloadmatches);

		// change size of iframe to fit content
		// jwcdg.cobacose.resizeIFrame($(document.body).height());
		// console.log('iframe height: ' + $(document.body).height());
	}

	function downloadMatchedFiles() {
		// set data
		$('#prefix').val(fileprefix);
		$('#suffix').val(filesuffix);
		$('#file').val(matchedfiles.join('|_|'));

		// remove old download_frame (why?)
		$('#download_frame').remove();
		// add new download_frame
		$('body').append(
				'<iframe id="download_frame" style="display:none;" src=""');

		// start download
		$('#download_matches_form').submit();
	}

	/*
	 * Function to zoom into the svg images. target references the UI control
	 * "+"
	 */
	function zoomIn(target) {
		var image = $(target.parentNode).children('object');
		image.attr('width', (parseFloat(image.attr('width')) + 10) + '%');
		image.attr('height', (parseFloat(image.attr('height')) + 10) + '%');
	}

	/*
	 * Function to zoom out of the svg images. target references the UI control
	 * "-"
	 */
	function zoomOut(target) {
		var image = $(target.parentNode).children('object');
		image.attr('width', (parseFloat(image.attr('width')) - 10) + '%');
		image.attr('height', (parseFloat(image.attr('height')) - 10) + '%');
	}

	/*
	 * Function to build the navigation bar which is located over the shown
	 * results
	 */
	function buildResultNavigation() {
		var navigationbar, fragment, config, jumpfirst, jumpleft, jumplast, jumpright, offset, pagemod, page, i, j;
		// construct the backwards navigation arrows
		fragment = document.createDocumentFragment();

		config = {
			id : 'firstpage',
			href : '#',
			onclick : onclick = 'jwcdg.results.setPage(' + 0 + ')'
		};
		jumpfirst = createElementHelper('a', config);
		jumpfirst.appendChild(document.createTextNode('<<'));
		fragment.appendChild(jumpfirst);
		fragment.appendChild(document.createTextNode(' '));

		config = {
			id : 'jumpleft',
			href : '#',
			onclick : 'jwcdg.results.setPage('
					+ Math.max(0, currentPage - PAGEJUMP) + ')'
		};
		jumpleft = createElementHelper('a', config);
		jumpleft.appendChild(document.createTextNode('<'));
		fragment.appendChild(jumpleft);
		fragment.appendChild(document.createTextNode(' '));

		// Add the pages that can be directly navigated
		offset = Math.floor(PAGEJUMP / 2);
		for (i = Math.max(currentPage - offset, 0), j = Math.min(currentPage
				+ offset, maximumPage); i <= j; i++) {
			if (i === currentPage) {
				config = {
					id : 'p' + i,
					href : '#',
					onclick : 'jwcdg.results.setPage(' + i + ')'
				};

				page = createElementHelper('a', config);
				// Because the textnode can't be styled direcly it is wrapped
				// into another element (<span>) to apply the bodl style for the
				// current page
				pagemod = document.createElement('span');
				pagemod.setAttribute('style', 'font-weight:bold');
				pagemod.appendChild(document.createTextNode((i + 1)));
				page.appendChild(pagemod);
				fragment.appendChild(page);
				fragment.appendChild(document.createTextNode(' '));

			} else {
				config = {
					id : 'p' + i,
					href : '#',
					onclick : 'jwcdg.results.setPage(' + i + ')'
				};
				page = createElementHelper('a', config);
				page.appendChild(document.createTextNode((i + 1)));
				fragment.appendChild(page);
				fragment.appendChild(document.createTextNode(' '));

			}
		}

		// construct the forwards navigation arrows
		config = {
			id : 'jumpright',
			href : '#',
			onclick : 'jwcdg.results.setPage('
					+ Math.min(maximumPage, currentPage + PAGEJUMP) + ')'
		};
		jumpright = createElementHelper('a', config);
		jumpright.appendChild(document.createTextNode('>'));
		fragment.appendChild(jumpright);
		fragment.appendChild(document.createTextNode(' '));

		config = {
			id : 'lastpage',
			href : '#',
			onclick : 'jwcdg.results.setPage(' + maximumPage + ')'
		};
		jumplast = createElementHelper('a', config);
		jumplast.appendChild(document.createTextNode('>>'));
		fragment.appendChild(jumplast);

		// delete the old navbar, then add the Fragment with the new nav bar
		navigationbar = $('#navigationbar');
		navigationbar.empty();
		navigationbar.append(fragment);
	}

	
	// function to create the archivestring which is used to request and archive
	// the
	// .cda file + the images for the different parses.
	function buildArchiveString(archivefiles, filename) {
		var archivelink, prefix, datastring, dlText;
		
		archivelink = document.createElement('a');
		prefix = 'ArchiveServlet?' + archivefiles.shift()
				+ archivefiles.shift() + '&data=';
		datastring = archivefiles.length === 1 ? archivefiles[0]
				: archivefiles.join(encodeURIComponent('|_|'));
		archivelink.setAttribute('href', prefix + datastring);
		archivelink.setAttribute('download', filename.substring(0,
				filename.length - 4)
				+ '.zip');
		dlText = document.createTextNode('Download All');
		archivelink.appendChild(dlText);
		return archivelink;
	}

	function updateProgress() {
		prog += STEPSIZE / diff * 100;
		parent.jwcdg.cobacose.updateProgress(prog);
	}

	// function to create the header/div pairs representing the results
	function createResultUI(hText, sentence, tabs, archivelink, file) {
		var result, config, header, div, filelink;

		result = [];
		// create header
		config = {
			'class' : 'head ui-accordion-header ui-helper-reset ui-state-default ui-corner-all'
		};
		header = createElementHelper('h3', config);
		header.appendChild(document.createTextNode(hText));
		// create div
		config = {
			'class' : 'result ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom',
			width : '100%',
			height : '100%'
		};
		div = createElementHelper('div', config);
		div.appendChild(document.createTextNode(sentence));
		div.appendChild(document.createElement('br'));
		div.appendChild(document.createElement('br'));
		div.appendChild(tabs);
		// create downloadlink for the matched .cda file
		config = {
			href : 'FileServlet?file=' + file,
			download : file
		};
		filelink = createElementHelper('a', config);
		filelink.appendChild(document.createTextNode('Download File'));
		// append links for the .cda file and for the archive containing the
		// .cda + images
		div.appendChild(filelink);
		div.appendChild(document.createElement('br'));
		div.appendChild(document.createElement('br'));
		div.appendChild(archivelink);
		result[0] = header;
		result[1] = div;
		return result;
	}

	function createElementHelper(type, config) {
		var element, attribute;
		// create the element , e.g 'a' (anchor)
		element = document.createElement(type);

		// configure the element , e.g. href="linktosomewhere"
		for (attribute in config) {
			if (config.hasOwnProperty(attribute)) {
				element.setAttribute(attribute, config[attribute]);
			}
		}

		return element;
	}

	function handleError(error) {

		finished = true;
		parent.jwcdg.cobacose.handleError(error);
	}

	function createTabHeaders(filename, length) {
		var ul, li, a, i;
		ul = document.createElement('ul');

		for (i = 0; i < length; i++) {
			li = document.createElement('li');
			a = document.createElement('a');
			a.setAttribute('href', '#' + filename + '-' + i);
			a.appendChild(document.createTextNode('Result ' + (i + 1)));
			li.appendChild(a);
			ul.appendChild(li);
		}
		return ul;
	}

	function createTabContent(filename, words, result, tabnumber) {
		var config, imagedata, tabcontent, zoomInButton, zoomOutButton, image, imageDl;

		imagedata = 'ImageServlet?' + 'words=' + encodeURIComponent(JSON.stringify(words))
				+ '&parse=' + encodeURIComponent(JSON.stringify(result));

		config = {
			width : '100%',
			height : '100%',
			style : 'overflow:auto',
			id : filename + '-' + tabnumber
		};
		tabcontent = createElementHelper('div', config);
		// add the edges where the constraint applies
		for ( var i = 0; i < result['edges'].length; i++) {
			tabcontent.appendChild(document.createTextNode(result['edges'][i]));
			tabcontent.appendChild(document.createElement('br'));
		}
		tabcontent.appendChild(document.createElement('br'));

		// create zoom in/out controls for the image
		config = {
			type : 'button',
			value : '+',
			onclick : 'jwcdg.results.zoomIn(this)'
		};
		zoomInButton = createElementHelper('input', config);

		config = {
			type : 'button',
			value : '-',
			onclick : 'jwcdg.results.zoomOut(this)'
		};
		zoomOutButton = createElementHelper('input', config);
		tabcontent.appendChild(zoomInButton);
		tabcontent.appendChild(zoomOutButton);
		tabcontent.appendChild(document.createElement('br'));

		config = {
			type : 'image/svg+xml;charset=utf-8',
			width : '100%',
			height : '100%',
			'data-image' : imagedata
		};
		// create the image
		image = createElementHelper('object', config);
		tabcontent.appendChild(image);
		tabcontent.appendChild(document.createElement('br'));

		// create a download link for the image
		imageDl = document.createElement('a');
		imageDl.setAttribute('href', imagedata);
		imageDl.setAttribute('download', 'image' + tabnumber + '.svg');
		imageDl.appendChild(document.createTextNode('Download Image'));
		tabcontent.appendChild(imageDl);
		return tabcontent;

	}

	function processData(data) {
		var fileaffixes, config, outer, outerlength, output, archivefiles, archivelink, preview, inner, innerlength, tabheaders, tabs, tabcontainer, combinedresult, result, filename, sentence, filenumber;

		if ($.isArray(data)) {
			if (data.length > 0) {
				// read all combinedresults

				for (outer = 0, outerlength = data.length; outer < outerlength; outer++) {

					combinedresult = data[outer];
					filename = combinedresult['file'];
					sentence = combinedresult['words'].join(' ');
					preview = sentence.split(' ', 5);
					tabs = [];
					tabheaders = createTabHeaders(filename,
							combinedresult['results'].length);
					archivefiles = [ 'file=' + filename,
							'&words='+encodeURIComponent(JSON.stringify(combinedresult['words'])) ];
					
					// read all results
					for (inner = 0,
							innerlength = combinedresult['results'].length; inner < innerlength; inner++) {
						result = combinedresult['results'][inner];
						tabs[inner] = createTabContent(filename, combinedresult['words'],
								result, inner);
						archivefiles[inner + 2] = encodeURIComponent(JSON
								.stringify(result));
					}
					archivelink = buildArchiveString(archivefiles, filename);

					// create container div to hold the tabs
					config = {
						width : '100%',
						height : '100%',
						id : filename,
						'class' : 'tab'
					};
					tabcontainer = createElementHelper('div', config);
					tabcontainer.appendChild(tabheaders);
					for ( var i = 0; i < tabs.length; i++) {
						tabcontainer.appendChild(tabs[i]);
					}

					// Each output element contains a header (h3) and a div,
					// similiar to the jquery ui accordion
					// The divs each contain a jquery tab, which itself
					// consists of 1 parent div and a pair of link + div for
					// each tab

					output = createResultUI(filename + ': ' + preview.join(' ')
							+ ' ...', sentence, tabcontainer, archivelink,
							filename);
					// Add Results to the correct Pages
					if (resultPages.length === entriesPerPage
							* (maximumPage + 1)) {
						maximumPage++;
						buildResultNavigation();
					}

					resultPages.push(output);
					filenumber = filename.replace(/\D+/g, '');
					if (fileprefix === '')
					{
						fileaffixes = filename.split(/\d+/);
						fileprefix = fileaffixes[0];
						filesuffix = fileaffixes[1];
					}
					matchedfiles.push(filenumber);
				}
				// Update UI
				clearUI();
				displayResults();

			}
		} else {
			handleError(data);
		}

		updateProgress();
	}
	
	function stopSearch()
	{		
		finished = true;
	}
	
	// public API
	return {
		zoomIn : zoomIn,
		zoomOut : zoomOut,
		setPage : setPage,
		downloadMatchedFiles : downloadMatchedFiles,
		stopSearch: stopSearch
	};

}(jQuery));