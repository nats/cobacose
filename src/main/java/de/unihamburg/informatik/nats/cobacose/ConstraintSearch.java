/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unihamburg.informatik.nats.cobacose;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.antlr.runtime.RecognitionException;

import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.CursorConfig;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.OperationStatus;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;

public class ConstraintSearch
{

    // String used to format the edges of the results
    // e.g. : 1 - Online-Petition -- OBJA --> 30 - unterzeichnet
    private final static String formatString = "%d - %s -- %s --> %d - %s";
    // Contains pairs of Filename, Sentence
    private Database corpus;

    /**
     * @param config
     *            The configuration
     * @param corpus
     *            The corpus
     * @param gram
     *            The grammar
     * @param Lexicon
     *            The lexicon, can be null
     **/
    public ConstraintSearch(Database corpus) throws IOException,
            RecognitionException
    {
        this.corpus = corpus;
    }

    static ExecutorService tpe = Executors.newCachedThreadPool();

    private class ConstraintEvaluatorCallable implements
            Callable<CombinedResult>
    {
        Constraint constraint;
        DecoratedParse dp;
        String key;

        public ConstraintEvaluatorCallable(String key, Constraint constraint,
                DecoratedParse parse)
        {
            this.key = key;
            this.constraint = constraint;
            this.dp = parse;
        }

        @Override
        public CombinedResult call() throws Exception
        {
            // process the detected constraint violations
            CombinedResult result;
            List<ConstraintViolation> violations = dp
                    .evalConstraint(constraint);

            result = createResult(key, dp, violations);
            return result;
        }

    }

    /**
     * 
     * @param con
     *            The constraint
     * @param to
     * @param from
     * @return Every Sentence where the constraint is found
     */
    public List<CombinedResult> searchForConstraintInCorpus(Constraint con,
            int from, int to)
    {
        ArrayList<CombinedResult> result = new ArrayList<CombinedResult>();

        try
        {
            // If we want to process only parts of the corpus we have to
            // reconstruct the keys to receive a suitable submap
            Queue<ConstraintEvaluatorCallable> tasks = new LinkedList<ConstraintEvaluatorCallable>();

            // Open Cursor
            CursorConfig cc = new CursorConfig();
            Cursor cursor = corpus.openCursor(null, cc);
            DatabaseEntry key = new DatabaseEntry();
            DatabaseEntry data = new DatabaseEntry();

            // Create key of first entry of the requests
            cursor.getFirst(key, data, null);
            String sample = StringBinding.entryToString(key);
            String[] components = sample.split("\\d+");
            String first = components[0] + from + components[1];

            // Process Entries within the bounds of the request
            StringBinding.stringToEntry(first, key);
            cursor.getSearchKey(key, data, null);
            processEntry(con, tasks, key, data);
            int counter = from;
            List<Future<CombinedResult>> res = new ArrayList<Future<CombinedResult>>();
            while (cursor.getNext(key, data, null) != OperationStatus.NOTFOUND
                    && counter < to)
            {
                counter++;
                processEntry(con, tasks, key, data);
                res.add(tpe.submit(tasks.poll()));
            }
            cursor.close();

            // go through all processed tasks and add the results to the list
            for (Future<CombinedResult> f : res)
            {
                CombinedResult r = f.get();
                if (!r.getResults().isEmpty())
                {
                    result.add(r);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return result;
    }

    private void processEntry(Constraint con,
            Queue<ConstraintEvaluatorCallable> tasks, DatabaseEntry key,
            DatabaseEntry data) throws IOException
    {
        try
        {
            // read the parse from the database
            ByteArrayInputStream bis = new ByteArrayInputStream(data.getData());
            ObjectInputStream ois = new ObjectInputStream(bis);
            DecoratedParse dp = (DecoratedParse) ois.readObject();

            String s = StringBinding.entryToString(key);
            // apply the constraint
            tasks.add(new ConstraintEvaluatorCallable(s, con, dp));

        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    /*
     * Method to construct the result object. String contains filename,
     * constraint, sentence, labels, Edges causing the violation
     */
    private static CombinedResult createResult(String filename, Parse sentence,
            List<ConstraintViolation> violations)
    {
        // Add shared information (filename, sentence) to the combinedresult
        CombinedResult result = new CombinedResult();
        result.setFile(filename);
        result.setWords(sentence.getWords());
        
        // add results containing individual information (e.g. edges) to the
        // combined result
        for (ConstraintViolation v : violations)
        {
            if (v != null)
            {
                Result r = new Result();
                r.setLevels(sentence.getLevels());
                Map<Integer, ArrayList<String>> nodes = new TreeMap<Integer, ArrayList<String>>();
                ArrayList<String> markedLevels = new ArrayList<String>();
                markedLevels.add("SYN");

                // Edge 1
                GraphemeNode dependent = v.getLV1().getDependentGN();
                GraphemeNode regent = v.getLV1().getRegentGN();
                if (!regent.isNil())
                {
                    r.setEdge(String.format(formatString, dependent.getNo(),
                            dependent.getArc().word,
                            sentence.getLabelOf(dependent.getNo(), "SYN"),
                            regent.getNo(), regent.getArc().word), 0);
                    nodes.put(dependent.getNo(), markedLevels);
                }
                else
                {
                    r.setEdge(String.format(formatString, dependent.getNo(),
                            dependent.getArc().word,
                            sentence.getLabelOf(dependent.getNo(), "SYN"), -1,
                            "NIL"), 0);
                }
                // Edge 2
                if (v.getLV2() != null)
                {
                    dependent = v.getLV2().getDependentGN();
                    regent = v.getLV2().getRegentGN();

                    if (!regent.isNil())
                    {
                        r.setEdge(String.format(formatString,
                                dependent.getNo(), dependent.getArc().word,
                                sentence.getLabelOf(dependent.getNo(), "SYN"),
                                regent.getNo(), regent.getArc().word), 1);
                        nodes.put(dependent.getNo(), markedLevels);
                    }
                    else
                    {
                        r.setEdge(String.format(formatString,
                                dependent.getNo(), dependent.getArc().word,
                                sentence.getLabelOf(dependent.getNo(), "SYN"),
                                -1, "NIL"), 0);
                    }
                }
                
                r.setLabels(sentence.getVerticesLabels());
                r.setIndices(sentence.getVerticesStructure().get("SYN"));
                r.setRef(sentence.getVerticesStructure().get("REF"));
                r.setNodes(nodes);
                result.addResult(r);
            }
        }
        return result;
    }

    @Override
    protected void finalize() throws Throwable
    {
        super.finalize();
        // close database connection
        corpus.close();
        corpus.getEnvironment().close();
    }

}
