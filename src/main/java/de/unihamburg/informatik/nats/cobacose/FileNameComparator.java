/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unihamburg.informatik.nats.cobacose;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator used to compare Alphahanumeric Filenames only by the Integer Values within the filenames
 * @author pfolleher
 *
 */
public class FileNameComparator implements Comparator<byte[]>, Serializable
{    
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(byte[] filename1, byte[] filename2)
    {
                
        int retval = 0;
        
        try
        {
            String s1 = new String(filename1, "UTF-8");
            String s2 = new String(filename2, "UTF-8");
            //strip any non-numeric character from the strings
            s1 = s1.replaceAll("\\D", "");
            s2 = s2.replaceAll("\\D", "");
            
            retval = Integer.compare(Integer.parseInt(s1), Integer.parseInt(s2));
        }
        catch (Exception e)
        {            
            e.printStackTrace();
        }
        
        
        
        return retval;
    }
}
