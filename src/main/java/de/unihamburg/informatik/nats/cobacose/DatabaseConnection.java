package de.unihamburg.informatik.nats.cobacose;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.Queue;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.log4j.Logger;

import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

public class DatabaseConnection
{
    private final static String fileEncoding = "UTF8";
    private static Logger logger = Logger.getLogger(DatabaseConnection.class);

    static public Database openDB(String corpuspath,
            GrammarEvaluator evaluator, Lexicon lex, Configuration config)
            throws IOException, RecognitionException
    {
        File serialized = new File(corpuspath + "/corpus");
        File reindex = new File(corpuspath + "/cobacose.reindex");
        Database db = null;

        if (reindex.exists())
        {
            logger.info(reindex.getName()
                    + " detected - starting to rebuild DB");
            reindex.delete();
            serialized.delete();
        }

        if (!serialized.exists())
        {
            buildDBfromCorpus(serialized, corpuspath, evaluator, lex, config);
        }

        // the database connection used is opened as read only for better
        // performance
        db = openReadOnlyConnection(serialized);
        return db;
    }

    static private Database openReadOnlyConnection(File serialized)
            throws IOException
    {
        EnvironmentConfig rEC = new EnvironmentConfig();
        rEC.setReadOnly(true);
        rEC.setAllowCreate(true);
        rEC.setTransactional(true);
        // Cachesize in Bytes, should be 0.5 GB
        rEC.setCacheSize(1024 * 1024 * 512);

        DatabaseConfig rDC = new DatabaseConfig();        
        rDC.setTransactionalVoid(true);
        rDC.setReadOnlyVoid(true);
        rDC.setBtreeComparator(new FileNameComparator());

        Environment env = new Environment(serialized, rEC);
        Database db = env.openDatabase(null, "corpus", rDC);

        return db;
    }

    static private Database openRWConnection(File serialized)
            throws IOException
    {
        EnvironmentConfig wrEC = new EnvironmentConfig();
        wrEC.setAllowCreate(true);
        
        DatabaseConfig wrDC = new DatabaseConfig();
        wrDC.setAllowCreate(true);        
        wrDC.setDeferredWrite(true);
        wrDC.setBtreeComparator(new FileNameComparator());

        serialized.mkdir();
        Environment env = new Environment(serialized, wrEC);
        Database db = env.openDatabase(null, "corpus", wrDC);
        
        return db;
    }

    static private void buildDBfromCorpus(File serialized, String corpuspath,
            GrammarEvaluator evaluator, Lexicon lex, Configuration config)
    {
        try
        {
            // create database
            Database db = openRWConnection(serialized);

            // walk over all files in the corpus directory
            File directory = new File(corpuspath);
            Queue<File> filequeue = new LinkedList<File>();
            if (directory.isDirectory())
            {
                filequeue.add(directory);
                while (!filequeue.isEmpty())
                {
                    File f = filequeue.poll();
                    String[] files = f.list();
                    for (String s : files)
                    {

                        if (s.endsWith(".cda"))
                        {
                            try 
                            {
                                parseCDA(evaluator, lex, config, db, f, s);
                            }
                            catch(RecognitionException e)
                            {
                                logger.warn("Recognition-Error: " + s);
                                logger.error(e.getMessage());
                            }
                        }
                        else if (s.endsWith(".conll"))
                        {
                            parseCONLL(evaluator, lex, config, db, f, s);
                        }
                        else
                        {
                            // check if current file is another directory we
                            // need to check
                            File maybedir = new File(f.getPath() + "/" + s);
                            if (maybedir.isDirectory())
                            {
                                filequeue.add(maybedir);

                            }
                        }

                    }                    
                }
                db.close();
                db.getEnvironment().close();
            }
        }
        catch (IOException e)
        {
            logger.warn("IO-Error");
            logger.error(e.getMessage());
        }
        
    }

    //ToDO: Prevent NULLPOINTER EXCEPTION
    private static void parseCONLL(GrammarEvaluator evaluator, Lexicon lex,
            Configuration config, Database db, File f, String s) throws IOException
    {
        String sentence = readFileAsString(f.getPath() + "/" + s);
        
        Parse p = Parse.fromConll(sentence);
        
        DecoratedParse dp = null;

        if (lex != null)
        {
            dp = p.decorate(evaluator, lex, config);
        }
        else
        {
            dp = p.decorate(evaluator, null, config);
        }        
        addEntryToDB(db, s, dp);
    }

    static private void parseCDA(GrammarEvaluator evaluator, Lexicon lex,
            Configuration config, Database db, File f, String s)
            throws IOException, RecognitionException
    {
        String sentence = readFileAsString(f.getPath() + "/" + s);
                        
        jwcdggrammarParser gParser = new jwcdggrammarParser(
                new CommonTokenStream(new jwcdggrammarLexer(
                        new ANTLRStringStream(sentence))));

        Parse p = gParser.annoEntry();
        DecoratedParse dp = null;

        if (lex != null)
        {
            dp = p.decorate(evaluator, lex, config);
        }
        else
        {
            dp = p.decorate(evaluator, null, config);
        }        
        addEntryToDB(db, s, dp);
    }    

    static private void addEntryToDB(Database db, String s, DecoratedParse dp)
    {
        DatabaseEntry key = new DatabaseEntry();
        DatabaseEntry data = null;

        try
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);

            oos.writeObject(dp);
            oos.flush();

            data = new DatabaseEntry(bos.toByteArray());            
            StringBinding.stringToEntry(s, key);            
            db.put(null, key, data);

            oos.close();
            bos.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /*
     * returns the content of a given file as string
     */
    private static String readFileAsString(String filePath)
            throws java.io.IOException
    {
        StringBuffer fileData = new StringBuffer(1000);

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(filePath), fileEncoding));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1)
        {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }
}
