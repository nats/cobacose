/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unihamburg.informatik.nats.cobacose;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;

import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sleepycat.je.Database;

import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaFalse;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaImpl;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/CSServlet" }, loadOnStartup = 1, asyncSupported = true)
public class CSServlet extends HttpServlet
{
    private ConstraintSearch cs;
    private int corpus_size;
    private final static String fileEncoding = "UTF8";
    private static Logger logger = Logger.getLogger(CSServlet.class);

    @Override
    /**
     * Assumption made is that any resources will be placed in the res folder (or any subfolder thereof). 
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        long start = System.currentTimeMillis();
        logger.info("Starting ConstraintSearch Servlet");
        Properties servletproperties = new Properties();
        try
        {
            // Read Servlet Properties
            InputStream sp = this.getClass().getClassLoader()
                    .getResourceAsStream("csservlet.properties");
            servletproperties.load(sp);

            String configname = servletproperties.getProperty("config");
            // Read jwcdg properties
            InputStream conf = this.getClass().getClassLoader()
                    .getResourceAsStream(configname);
            Properties confprops = new Properties();
            confprops.load(conf);
            Configuration configuration = new Configuration(confprops);

            // Construct Constraint search object
            Grammar grammar = readGrammar(
                    configuration.getString("grammarFiles"), configuration);

            String corp = servletproperties.getProperty("corpus");
            
            String corpuspath = "";
            try {
                    corpuspath = Thread.currentThread().getContextClassLoader()
                    .getResource(corp).getPath();
            }
            catch(NullPointerException e)
            {
                throw new FileNotFoundException("Corpus not found. Please check the settings in the csservlet.properties file and check if the directory exists.");
            }

            boolean useLexicon = servletproperties.getProperty("lexicon")
                    .equalsIgnoreCase("true");
            Lexicon lexicon = null;
            if (useLexicon)
            {
                File lexfile = new File(Thread.currentThread()
                        .getContextClassLoader()
                        .getResource("deutsch-lexikon.cdg").getPath());

                lexicon = Lexicon.fromFile(lexfile,
                        configuration.getString("grammarEncoding"),
                        grammar.getAllAttributes());
            }
            GrammarEvaluator evaluator = new CachedGrammarEvaluator(grammar,
                    configuration);

            Database db = DatabaseConnection.openDB(corpuspath, evaluator, lexicon, configuration);
            corpus_size = (int) db.count();
            cs = new ConstraintSearch(db);

        }
        catch (Exception e2)
        {
            e2.printStackTrace();
        }

        long end = System.currentTimeMillis();
        long diff = end - start;
        logger.info(diff + " ms");

    }
    
    /*
     * Method to read one or more grammar files
     */
    private Grammar readGrammar(String name, Configuration config)
            throws RecognitionException, IOException
    {
        String prefix = this.getClass().getClassLoader().getResource("")
                .getPath()
                + "/";
        StringBuilder sb = new StringBuilder();
        String[] files = name.split(",");
        StringReader sr;

        // we have to check the return value of split before we attempt to read
        // the grammar
        if (files != null)
        {
            for (int i = 0; i < files.length; i++)
            {
                sb.append(readFileAsString(prefix + files[i]));
            }

            sr = new StringReader(sb.toString());
        }
        else
        {
            sr = new StringReader(prefix + readFileAsString(name));
        }

        Grammar grammar = Grammar.fromFile(sr, config);
        return grammar;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        final AsyncContext act = req.startAsync(req, resp);
        // negative timeout indicates no timeout at all
        act.setTimeout(-1);
        act.start(new Runnable()
        {
            @Override
            public void run()
            {
                ServletRequest req = act.getRequest();
                ServletResponse resp = act.getResponse();
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");

                // read contraint from request
                String constraintdesc = req.getParameter("constraint");
                String from = req.getParameter("from");
                String to = req.getParameter("to");

                try
                {
                    try
                    {
                        VerifiedInput vi = checkInputs(from, to, constraintdesc);

                        // Perform Constraint Search
                        logger.info("Starting Constraint Search!");

                        List<CombinedResult> answer = cs
                                .searchForConstraintInCorpus(
                                        vi.getConstraint(), vi.getFrom(),
                                        vi.getTo());
                        // Send results as JSON string
                        ObjectMapper mapper = new ObjectMapper();
                        resp.getWriter().write(
                                mapper.writeValueAsString(answer));
                        resp.getWriter().flush();

                    }
                    catch (CobacoseInputException cie)
                    {
                        ObjectMapper mapper = new ObjectMapper();
                        resp.getWriter().write(mapper.writeValueAsString(cie));
                    }
                }
                catch (IOException io)
                {
                    io.printStackTrace();
                }

                act.complete();

            }
        });

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write("" + corpus_size);
    }

    /*
     * returns the content of a given file as string
     */
    private static String readFileAsString(String filePath)
            throws java.io.IOException
    {
        StringBuffer fileData = new StringBuffer(1000);

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(filePath), fileEncoding));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1)
        {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }

    private VerifiedInput checkInputs(String from, String to, String constraint)
            throws CobacoseInputException
    {
        // Verify that from / to are numbers within the limits
        int f = 0;
        int t = 0;
        if (!from.equals("") && !from.matches("\\D"))
        {
            f = Integer.valueOf(from);
        }
        else
        {
            throw new CobacoseInputException("from is not a number or empty.");
        }

        if (!to.equals("") && !to.matches("\\D"))
        {
            t = Integer.valueOf(to);
        }
        else
        {
            throw new CobacoseInputException("to is not a number or empty.");
        }

        if (f < 1 || f > t)
        {
            throw new CobacoseInputException(
                    "from is outside the limits of [1, to]");
        }

        if (t > corpus_size || t < f)
        {
            throw new CobacoseInputException(
                    "to is outside the limits of [from," + corpus_size + "]");
        }

        // Check if the constraint has a signature
        if (!constraint.contains(":"))
        {
            // checkFormula(constraint);

            // complete the constraint by adding a signature to the
            // given formula
            StringBuilder sb = new StringBuilder();
            if (constraint.contains("Y") && constraint.contains("X"))
            {
                sb.append("{X:SYN,Y:SYN} :");
                sb.append("'" + System.currentTimeMillis() + "' : ");
                sb.append("proj : 1.0 : ");
            }
            else if (constraint.contains("X"))
            {
                sb.append("{X:SYN} :");
                sb.append("'" + System.currentTimeMillis() + "' : ");
                sb.append("proj : 1.0 : ");
            }

            constraint = sb.toString() + constraint;
        }

        if (!constraint.endsWith(";"))
        {
            constraint += ";";
        }

        jwcdggrammarParser cParser = new jwcdggrammarParser(
                new CommonTokenStream(new jwcdggrammarLexer(
                        new ANTLRStringStream(constraint))));

        Constraint con = null;
        try
        {
            con = cParser.constraint().c;
            con.formula = new FormulaImpl(con.formula, new FormulaFalse());

        }
        catch (RecognitionException re)
        {
            throw new CobacoseInputException(
                    "Constraint not recognized.\n"
                            + cParser.getErrorHeader(re)
                            + " "
                            + cParser.getErrorMessage(re,
                                    jwcdggrammarParser.tokenNames) + "-"
                            + re.token.getText());
        }

        return new VerifiedInput(f, t, con);

    }

    

    

    

    
}
