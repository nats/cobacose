package de.unihamburg.informatik.nats.cobacose;

public class CobacoseInputException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    public CobacoseInputException(String msg)
    {
        super(msg);
    }
}
