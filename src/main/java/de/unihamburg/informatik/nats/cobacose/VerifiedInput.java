package de.unihamburg.informatik.nats.cobacose;

import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;

public class VerifiedInput
{

    private Constraint constraint;
    private int from;
    private int to;
    
    public VerifiedInput(int f, int t, Constraint con)
    {
        from = f;
        to = t;
        constraint = con;
    }

    public Constraint getConstraint()
    {
        return constraint;
    }

    public void setConstraint(Constraint constraint)
    {
        this.constraint = constraint;
    }

    public int getFrom()
    {
        return from;
    }

    public void setFrom(int from)
    {
        this.from = from;
    }

    public int getTo()
    {
        return to;
    }

    public void setTo(int to)
    {
        this.to = to;
    }

}
