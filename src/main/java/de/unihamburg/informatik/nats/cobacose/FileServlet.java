/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unihamburg.informatik.nats.cobacose;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/FileServlet" }, loadOnStartup = 1, asyncSupported = true)
public class FileServlet extends HttpServlet
{
    private String corpusPath;
    
    @Override
    public void init() throws ServletException
    {
        Properties servletproperties = new Properties();
        
     // Read Servlet Properties
        InputStream sp = this.getClass().getClassLoader()
                .getResourceAsStream("csservlet.properties");
        try
        {
            servletproperties.load(sp);
            String corp = servletproperties.getProperty("corpus");
            corpusPath = Thread.currentThread().getContextClassLoader()
                    .getResource(corp).getPath();
        }
        catch (IOException e)
        {            
            e.printStackTrace();
        }
        
        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException
    {
        final AsyncContext act = req.startAsync(req, resp);
        act.setTimeout(-1);
        act.start(new Runnable()
        {
            @Override
            public void run()
            {

                try
                {
                    ServletRequest req = act.getRequest();
                    ServletResponse resp = act.getResponse();
                    resp.setCharacterEncoding("UTF-8");

                    //no assumptions are made about the corpus structure,
                    //so we need to search for the actual file within the corpus
                    File f = new File(corpusPath);
                    String filePath = "";
                    String fileName = req.getParameter("file");                    
                    Queue<File> filequeue = new LinkedList<File>();
                    filequeue.add(f);
                    
                    while(!filequeue.isEmpty())
                    {
                        File file = filequeue.poll();
                        
                        if(file.getName().equals(fileName))
                        {
                            filePath = file.getAbsolutePath();                            
                            
                            //Check that the found file actually lies below the corpus path to avoid security issues
                            if(!filePath.contains(f.getAbsolutePath()))
                            {
                               throw new IOException("Invalid Filepath. File has to be below the corpus Path");
                            }
                            break;
                        }
                        else if(file.isDirectory())
                        {
                            for(File g : file.listFiles())
                            {
                                filequeue.add(g);
                            }
                        }
                        
                    }
                    
                    if(filePath.equals(""))
                    {
                        throw new FileNotFoundException("The file " + fileName + " could not be found");
                    }
                    
                    
                    //read the file then send it to the client
                    InputStream is = new FileInputStream(filePath);
                    byte[] bytes = IOUtils.toByteArray(is);
                    resp.getOutputStream().write(bytes);
                    resp.getOutputStream().flush();
                    resp.getOutputStream().close();
                    
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                act.complete();
            }
        });

    }

    
}