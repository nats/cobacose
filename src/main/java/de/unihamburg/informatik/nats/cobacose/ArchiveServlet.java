package de.unihamburg.informatik.nats.cobacose;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/ArchiveServlet" }, loadOnStartup = 1, asyncSupported = true)
public class ArchiveServlet extends HttpServlet
{
    private String baseURL = "";
    private static Logger logger = Logger.getLogger(ArchiveServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        final AsyncContext act = req.startAsync(req, resp);
        act.setTimeout(-1);
        act.start(new Runnable()
        {
            @Override
            public void run()
            {

                try
                {
                    ServletRequest req = act.getRequest();
                    HttpServletResponse resp = (HttpServletResponse) act
                            .getResponse();
                    resp.setCharacterEncoding("UTF-8");
                    resp.setContentType("application/zip");
                    

                    // Construct base URL
                    if (baseURL.equals(""))
                    {
                        synchronized (baseURL)
                        {
                            baseURL = req.getScheme() + "://"
                                    + req.getServerName() + ":"
                                    + req.getLocalPort() + "/";
                        }
                    }

                    String file = req.getParameter("file");
                    String words = req.getParameter("words");
                    String rawdata = req.getParameter("data");
                    
                    resp.addHeader("Content-Disposition",
                            "attachment; filename=\""+ file.substring(0,file.length() - 3)+ "zip" +"\"");
                    
                    
                    String[] images;
                    if (rawdata != null)
                    {
                        images = rawdata.split("\\|_\\|");
                    }
                    else
                    {
                        images = new String[1];
                        images[0] = "";
                    }

                    // Create Archive and send it
                    ZipOutputStream zipper = new ZipOutputStream(resp
                            .getOutputStream());

                    ZipEntry cdafile = new ZipEntry(file);
                    zipper.putNextEntry(cdafile);
                    getFile(file, zipper);
                    zipper.closeEntry();

                    int counter = 0;
                    for (String s : images)
                    {
                        if (!s.equals(""))
                        {
                            ZipEntry img = new ZipEntry("image-" + counter
                                    + ".svg");
                            zipper.putNextEntry(img);
                            getImage(words, zipper, s);
                            zipper.closeEntry();
                            counter++;
                        }
                    }
                    zipper.close();

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                act.complete();
            }
        });
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        final AsyncContext act = req.startAsync(req, resp);
        act.setTimeout(-1);
        act.start(new Runnable()
        {
            @Override
            public void run()
            {

                try
                {
                    ServletRequest req = act.getRequest();
                    HttpServletResponse resp = (HttpServletResponse) act
                            .getResponse();
                    resp.setCharacterEncoding("UTF-8");
                    resp.setContentType("application/zip");
                    resp.addHeader("Content-Disposition",
                            "attachment; filename=\"matchedfiles.zip\"");

                    // Construct base URL
                    if (baseURL.equals(""))
                    {
                        synchronized (baseURL)
                        {
                            baseURL = req.getScheme() + "://"
                                    + req.getServerName() + ":"
                                    + req.getLocalPort() + "/";
                        }
                    }

                    // Read parameters
                    String filenames = req.getParameter("file");
                    String prefix = req.getParameter("prefix");
                    String suffix = req.getParameter("suffix");

                    String[] files = filenames.split("\\|_\\|");

                    // Create Archive and send it
                    ZipOutputStream zipper = new ZipOutputStream(resp
                            .getOutputStream());

                    for (String file : files)
                    {
                        String fullname = prefix + file + suffix;
                        // logger.info("filename: " + fullname);
                        ZipEntry cdafile = new ZipEntry(fullname);
                        zipper.putNextEntry(cdafile);
                        getFile(fullname, zipper);
                        zipper.closeEntry();
                    }

                    zipper.close();

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                act.complete();
            }
        });
    }

    // Request a File from the FileServlet and add it to the Queue
    private void getFile(String file, ZipOutputStream data) throws IOException,
            MalformedURLException
    {
        HttpURLConnection con = (HttpURLConnection) new URL(baseURL
                + "FileServlet?file=" + file).openConnection();
        InputStream is = con.getInputStream();

        copyFromStream(data, is);

    }

    // Request an Image from the ImageServlet and add it to the Queue
    private void getImage(String words, ZipOutputStream data, String s)
            throws IOException, MalformedURLException
    {        
        HttpURLConnection con = (HttpURLConnection) new URL(baseURL
                + "ImageServlet?parse=" + URLEncoder.encode(s, "UTF-8")
                + "&words=" + URLEncoder.encode(words, "UTF-8"))
                .openConnection();
        InputStream is = con.getInputStream();

        copyFromStream(data, is);

    }

    // Method to copy data from an InputStream to an (Zip)Outputstream
    private void copyFromStream(ZipOutputStream data, InputStream is)
    {
        int nb;
        byte[] buffer = new byte[1024];
        try
        {
            while ((nb = is.read(buffer)) != -1)
            {
                data.write(buffer, 0, nb);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
