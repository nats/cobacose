package de.unihamburg.informatik.nats.cobacose;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import de.unihamburg.informatik.nats.jwcdg.DefaultParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;


public class CorpusLexicalizer {

	private Parser parser;
	private GrammarEvaluator evaluator;
	
	public CorpusLexicalizer(Parser parser, GrammarEvaluator evaluator) {
		super();
		this.parser = parser;
		this.evaluator = evaluator;
	}
	
	/**
	 * reads all files identified by inPath, min and max as parses
	 * decorates them
	 * writes them to the location designated by outPath
	 * 
	 * in and outPath need to include exactly one %d as placeholder for the sentence number 
	 */
	public void enrichAnnotationsWithLexiconEntries(String inPath, String outPath, int min, int max) {
		
		for(int i = min; i <= max; i++) {
			Parse g;
			try {
				String inFileName = String.format( inPath , i);
				String outFileName = String.format(outPath, i);
				g = readAnno(inFileName);
				DecoratedParse goldDP = g.decorate(evaluator, parser.getLexicon(), parser.getConfig());
				String output = goldDP.toAnnotation(true);
				writeAnno(outFileName, output);
				
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			} catch (RecognitionException e) {
				e.printStackTrace();
				continue;
			}
			catch (CobacoseInputException e)
			{
			    e.printStackTrace();
			}
		}
	}

	/**
	 * Tries to read the given filename and parse it as a cda into a parse object.
	 * 
	 * Might fail for two different reasons as indicated by the throws statement:
	 * @throws IOException if the file could not be read
	 * @throws RecognitionException if the file contents does not confirm cda syntax
	 * @throws CobacoseInputException 
	 */
	private Parse readAnno(String pathName) throws IOException, RecognitionException, CobacoseInputException {
		System.out.println("Opening: " + pathName);
		String s = readFileAsString(pathName);
		jwcdggrammarParser gParser = new jwcdggrammarParser(new CommonTokenStream(new jwcdggrammarLexer(new ANTLRStringStream(s))));
		return gParser.annoEntry();
	}
	
	private void writeAnno(String pathName, String output) throws IOException {
		System.out.println("writing: " + pathName);
		FileOutputStream fos = new FileOutputStream(pathName);
		OutputStreamWriter osw = new OutputStreamWriter(fos,"iso-8859-15");
		osw.write(output);
		osw.flush();
		osw.close();
	}
	
	/**
	 * returns the content of a given file as string
	 */
    private static String readFileAsString(String filePath) throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
        		new FileInputStream(filePath),
        		"iso-8859-15"));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }
    
    public static void main(String[] args) {
    	Parser p;
		try {
			p = JWCDG.makeParser(Configuration.fromFile("/home/pfolleher/workspace/CS-webinterface/src/main/resources/corpusLexicalizer.properties"), new DefaultParserFactory());
			CorpusLexicalizer lexicalizer = new CorpusLexicalizer( p, 
					new CachedGrammarEvaluator(p.getGrammar(), p.getConfig()) );
			lexicalizer.enrichAnnotationsWithLexiconEntries(
					"/home/pfolleher/corpora/heise/014/auto-heiseticker-s%d.cda", 
					"/home/pfolleher/corpora/heise_lexed/014/heiseticker-s%d.cda", 
					149513, 
					149999);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}

