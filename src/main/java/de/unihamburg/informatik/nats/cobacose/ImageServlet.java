/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unihamburg.informatik.nats.cobacose;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unihamburg.informatik.nats.jwcdg.gui.DepTreeGenerator;


@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/ImageServlet" }, loadOnStartup = 1, asyncSupported = true)
public class ImageServlet extends HttpServlet
{
    private DepTreeGenerator dtg;
    
    @Override
    public void init() throws ServletException
    {

        //pv = new DepTreeViewer();
        dtg = new DepTreeGenerator();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException
    {
        final AsyncContext act = req.startAsync(req, resp);
        act.setTimeout(-1);
        act.start(new Runnable()
        {
            @Override
            public void run()
            {

                try
                {
                    ServletRequest req = act.getRequest();
                    ServletResponse resp = act.getResponse();
                    resp.setContentType("image/svg+xml");
                    resp.setCharacterEncoding("UTF-8");

                    //read parameter
                    ObjectMapper mapper = new ObjectMapper();
                    Result r = mapper.readValue(req.getParameter("parse"),
                            Result.class);
                    
                    List<String> words = mapper.readValue(req.getParameter("words"),new TypeReference<List<String>>(){});

                    //reconstruct the maps that were deconstructed when creating the result object
                    Map<String, List<Integer>> verticesStructure = new TreeMap<String, List<Integer>>();
                    verticesStructure.put("SYN", r.getIndices());
                    verticesStructure.put("REF", r.getRef());
                    
                    
                    dtg.writeTree(r.getLevels(),r.getLabels(),verticesStructure, words,r.getNodes(), resp.getWriter()  );
                    
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                act.complete();
            }
        });
    }
}