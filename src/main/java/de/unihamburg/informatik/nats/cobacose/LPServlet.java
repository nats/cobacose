/*  This file is part of cobacose.
 *  cobacose is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  cobacose is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with cobacose.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unihamburg.informatik.nats.cobacose;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.antlr.runtime.RecognitionException;

import de.unihamburg.informatik.nats.jwcdg.DefaultParserFactory;
import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/LPServlet" }, loadOnStartup = -1, asyncSupported = true)
public class LPServlet extends HttpServlet
{

    private final static String fileEncoding = "UTF8";
    private Parser parser;

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        Properties servletproperties = new Properties();
        try
        {
            InputStream sp = this.getClass().getClassLoader()
                    .getResourceAsStream("lpservlet.properties");
            servletproperties.load(sp);

            String configname = servletproperties.getProperty("config");
            // Read jwcdg properties
            InputStream conf = this.getClass().getClassLoader()
                    .getResourceAsStream(configname);
            Properties confprops = new Properties();
            confprops.load(conf);
            Configuration configuration = new Configuration(confprops);

            // Construct Parser
            Grammar grammar = readGrammar(
                    configuration.getString("grammarFiles"), configuration);

            File lexfile = new File(Thread.currentThread()
                    .getContextClassLoader().getResource("deutsch-lexikon.cdg")
                    .getPath());

            Lexicon lexicon = Lexicon.fromFile(lexfile,
                    configuration.getString("grammarEncoding"),
                    grammar.getAllAttributes());

            parser = makeParser(grammar, lexicon, configuration);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (RecognitionException e)
        {
            e.printStackTrace();
        }
    }

    private Parser makeParser(Grammar grammar, Lexicon lexicon,
            Configuration configuration)
    {
        DefaultParserFactory fac = new DefaultParserFactory();
        Parser p = fac.makeParser(grammar, lexicon, configuration);
        return p;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        final AsyncContext act = req.startAsync(req, resp);
        act.setTimeout(-1);
        act.start(new Runnable()
        {

            @Override
            public void run()
            {
                ServletRequest req = act.getRequest();
                ServletResponse resp = act.getResponse();
                resp.setContentType("text/plain");
                resp.setCharacterEncoding("UTF-8");

                String sentence = req.getParameter("sentence");
                String[] tokens = sentence.split(" ");

                //parse the sentence which was provided by the user
                if (tokens != null)
                {
                    ArrayList<String> tokenlist = new ArrayList<String>();
                    for (String t : tokens)
                    {
                        tokenlist.add(t);
                    }
                    Parse p = parser.parse(tokenlist);

                    try
                    {
                        resp.getWriter().write(p.print());
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                //TODO: Test single word case and maybe fix it
                act.complete();
            }
        });
    }

    /*
     * Method to read one or more grammar files
     */
    private Grammar readGrammar(String name, Configuration config)
            throws RecognitionException, IOException
    {
        String prefix = this.getClass().getClassLoader().getResource("")
                .getPath()
                + "/";
        StringBuilder sb = new StringBuilder();
        String[] files = name.split(",");
        StringReader sr;

        // we have to check the return value of split before we attempt to read
        // the grammar
        if (files != null)
        {
            for (int i = 0; i < files.length; i++)
            {
                sb.append(readFileAsString(prefix + files[i]));
            }

            sr = new StringReader(sb.toString());
        }
        else
        {
            sr = new StringReader(prefix + readFileAsString(name));
        }

        Grammar grammar = Grammar.fromFile(sr, config);
        return grammar;

    }

    /*
     * returns the content of a given file as string
     */
    private static String readFileAsString(String filePath)
            throws java.io.IOException
    {
        StringBuffer fileData = new StringBuffer(1000);

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(filePath), fileEncoding));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1)
        {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }
}
