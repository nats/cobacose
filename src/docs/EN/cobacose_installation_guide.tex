\documentclass[a4paper,10pt]{article}
%\documentclass[a4paper,10pt]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[hyphens]{url}
\usepackage[breaklinks=true]{hyperref}

\title{COBACOSE Manual}
\author{The CDG Team}
\date{\today}

\pdfinfo{%
  /Title    ()
  /Author   ()
  /Creator  ()
  /Producer ()
  /Subject  ()
  /Keywords ()
}

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage

\section{Installation}
The installation of the COBACOSE (COrpus BAsed COnstraint SEarch) software
requires the following steps:
\begin{enumerate}
 \item build the COBACOSE software using Maven
 \item deploy the COBACOSE software to your server
\end{enumerate}

\subsection{Building COBACOSE}
Before building the COBACOSE software resources need to be added. The COBACOSE
project uses the default structure for
\href{http://maven.apache.org/guides/introduction/introduction-to-the- \\
standard-directory-layout.html}{maven projects}. In the
\textit{source/main/resources} folder you will find the configuration for the
servlet and the jwcdg library. In the \textit{csservlet.properties} file the
corpus property should be set to the location of the corpus (see section
\ref{cs} for more details). 
\newline
After adding your corpus and configurating COBACOSE you need to manually build
the jwcdg library. After building jwcdg you should either add it to your local
maven repository or place it in the library folder of your server and adjust the
\textit{pom.xml} accordingly by setting the scope of jwcdg to provided. To add
jwcdg to your local maven repository you can follow these
\href{http://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html}{
instructions}.
Now everything should be ready, so you can build COBACOSE by invoking
\textit{mvn package} from the COBACOSE root directory. Depending on the server
you are using (and the availability of a plug-in) you might want to add
something to the command line as described in section \ref{deploy}.

\subsection{Deployment}
As the deployment steps depend on the server you are going to use, you should
refer to the documentation of your server. This manual will only provide an
outline of the deployment with examples based on the tomcat server.

\subsection{Configuring COBACOSE}
COBACOSE comes with several properties files that are used to configure the
different parts of the system. This section tries to explain which files are
used for what part of the system. Note that the properties files usually are
paired up, with one properties file being the ``default file'' and the other
handling special aspects and allowing you to overwrite default values without
messing up said default file.

\subsubsection{Configuring the constraint search servlet}\label{cs}
The constraint search servlet (in the following text called csservlet) is used
to search a corpus for sentences that match a constraint given by the user. To
configure the csservlet the following files are used:
\begin{itemize}
 \item \textbf{csservlet.properties}: The csservlet properties contains the
following options: \textbf{config}, \textbf{corpus} and \textbf{lexicon}.
\textbf{config} is the path to the configuration to be used by jwcdg.
\textbf{corpus} is the path to your corpus. This path is relative to the
\textbf{classpath} used by the server. This means if your corpus is called
MyCorpus and is placed in the servers shared library folder (e.g.
CATALINA\_HOME/lib for Tomcat) it is enough to specify your corpus by
\begin{verbatim}corpus = MyCorpus\end{verbatim}. \textbf{lexicon} specifies if
a lexicon should be used or not. If you don't want to use a lexicon you should
use the \textbf{CorpusLexicalizer} utility on your corpus first (NOTE:
CorpusLexicalizer is still in an experimental state and might cause issues).
\item \textbf{cobacose.properties}: The properties used by jwcdg. The only
noteworthy thing here should be the use of the \textbf{min\_deutsch.cdg} as the
grammar file. \textbf{min\_deutsch.cdg} is a minimal grammar that is required by
jwcdg to run. You don't need to use a complete grammar, as the only constraints
your going to work with are the ones entered into the webinterface.
\end{itemize}

\subsubsection{Configuring the live parser servlet}
The liveparser servlet (in the following text called lpservlet) is similar to
the command line tool of jwcdg. To configure the lpservlet the following files
are used:
\begin{itemize}
 \item \textbf{lpservlet.properties}: This file only contains the
\textbf{config} line, as there is no further need for configuration besides the
configuration used by jwcdg.
\item \textbf{liveparser.properties}: This file contains the configuration for
jwcdg.
\end{itemize}

\subsection{Configuring the Server}
The basic steps for the configuration should be described in the manual
provided by your server vendor. You should set the heap size to a sufficiently
large size and the URLs should be UTF-8 encoded.
\newline
To set the heap size in tomcat you should add the following line to your
setenv.sh or .bat: 
\begin{verbatim}
export CATALINA_OPTS="-Xms1536m -Xmx1536m -Xss256k"                    
\end{verbatim}
To set the URL encoding to UTF-8 you need to set the attribute of the connector
in the \textit{server.xml}: 
\begin{verbatim}
<Connector port="8080"
           protocol="HTTP/1.1"
           connectionTimeout="20000"
           redirectPort="8443"
           URIEncoding="UTF-8" /> 
\end{verbatim}

\subsection{Deployment}\label{deploy}
If a Maven plug-in is available for your server the deployment can be
automated. The required configuration steps should be documented in the
plug-ins documentation. Otherwise the deployment has to be performed manually.
Usually deployment only requires to copy the \textit{cobacose.war} to a certain
directory within your server. In case of the tomcat server that would be the
\textit{webapp} folder of the server. 

\subsubsection{Deployment with Tomcat Plugin}\label{tomcatplugin}
The following steps are required to deploy cobacose using the maven tomcat
plugin:
\begin{enumerate}
 \item create a \textit{user} in \textit{tomcat-users.xml} with manager
rights
\begin{verbatim}
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
  <role rolename="manager"/>
  <user username="test" password="test" roles="manager"/>
</tomcat-users>
\end{verbatim}

\item create a \textit{server} in settings.xml
\begin{verbatim}
<server>
        <id>mytomcat</id>
        <username>test</username>
        <password>test</password>
</server>
\end{verbatim}

\item add the plugin configuration for the tomcat maven plugin to your
\textit{pom.xml}. \textit{url} is the address of the manager application.
\textit{server} is the server defined in \textit{settings.xml} and \textit{path}
is the path to your application that you want to deploy.
\begin{verbatim}
<plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>tomcat7-maven-plugin</artifactId>
        <configuration>
                <url>http://192.168.1.7:8080/manager</url>
                <server>mytomcat</server>
                <path>/mywebapp</path>
        </configuration>
</plugin>
\end{verbatim}

\end{enumerate}
Now you can deploy anytime to the tomcat server using the command:
\begin{verbatim}
mvn tomcat7:deploy
\end{verbatim}



\end{document}
