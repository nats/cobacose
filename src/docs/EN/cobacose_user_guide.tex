\documentclass[a4paper,10pt]{article}
%\documentclass[a4paper,10pt]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[hyphens]{url}
\usepackage[breaklinks=true]{hyperref}

\title{COBACOSE Manual}
\author{The CDG Team}
\date{\today}

\pdfinfo{%
  /Title    ()
  /Author   ()
  /Creator  ()
  /Producer ()
  /Subject  ()
  /Keywords ()
}

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage

\section{Installation}
The installation of the COBACOSE software requires the following steps:
\begin{enumerate}
 \item build the COBACOSE software using Maven
 \item deploy the COBACOSE software to your server
\end{enumerate}

\subsection{Building COBACOSE}
Before building the COBACOSE software resources need to be added. The COBACOSE
project uses the default structure for projects as described here:
http://maven.apache.org/guides/introduction/introduction-to-the- \\
standard-directory-layout.html. So any resource that needs to be added (e.g.
your corpus) should be placed in the \textit{source/main/resources} folder. In
that folder you will also find the configuration for the servlet and the jwcdg
library. In the \textit{servlet.properties} file the corpuspath should be set to
the location of the corpus. If for example, your corpus is placed in a folder
named \textit{corpus} placed under the \textit{resources} folder, then the
corpuspath should be set to corpus.
\newline
After adding your corpus and configurating COBACOSE you need to manually build
the jwcdg library. After building jwcdg you should either add it to your local
maven repository or place it in the library folder of your server and adjust the
\textit{pom.xml} accordingly by setting the scope of jwcdg to provided. To add
jwcdg to your local maven repository you can follow these
\href{http://maven.apache.org/guides/mini/guide-3rd-party-jars-local.html}{
instructions}.
Now everything should be ready, so you can build COBACOSE by invoking
\textit{mvn package} from the COBACOSE root directory. Depending on the server
you are using (and the availability of a plug-in) you might want to add
something to the command line as described in section \ref{deploy}.

\subsection{Deployment}
As the deployment steps depend on the server you are going to use, you should
refer to the documentation of your server. This manual will only provide an
outline of the deployment with examples based on the tomcat server.

\subsection{Configuring the Server}
The basic steps for the configuration should be described in the manual
provided by your server vendor. You should set the heap size to a sufficiently
large size and the URLs should be UTF-8 encoded.
\newline
To set the heap size in tomcat you should add the following line to your
setenv.sh or .bat: 
\begin{verbatim}
export CATALINA_OPTS="-Xms1536m -Xmx1536m -Xss256k"                    
\end{verbatim}
To set the URL encoding to UTF-8 you need to set the attribute of the connector
in the \textit{server.xml}: 
\begin{verbatim}
<Connector port="8080"
           protocol="HTTP/1.1"
           connectionTimeout="20000"
           redirectPort="8443"
           URIEncoding="UTF-8" /> 
\end{verbatim}

\subsection{Deployment}\label{deploy}
If a Maven plug-in is available for your server the deployment can be
automated. The required configuration steps should be documented in the
plug-ins documentation. Otherwise the deployment has to be performed manually.
Usually deployment only requires to copy the \textit{cobacose.war} to a certain
directory within your server. In case of the tomcat server that would be the
\textit{webapp} folder of the server. 

\section{Usage}
This section describes the usage of the COBACOSE software.

\subsection{Web Interface}
To start the web interface open a browser window and go to
\textit{<yourserver>/cobacose/cs.html}. Figure \ref{startscreen} shows what the
web interface looks like when loaded. 
\begin{enumerate}
 \item the input for the constraint
 \item the lower limit of the search
 \item the upper limit of the search
 \item button to start the search
 \item progress (in \%)
 \item number of results displayed on the page
 \item online help
\end{enumerate}
\begin{figure}
 \frame{\includegraphics[width=1\textwidth]{images/cobacose_start.png}}
 \caption{The COBACOSE client}
 \label{startscreen}
\end{figure}
\textbf{Example Task: Try to find negations in the first part of a corpus.} 

Steps:
\begin{enumerate}
 \item enter the provided constraint (see below) into \textbf{(1)}
 \item enter lower and upper search limits (e.g. 1, 9999) into \textbf{(2,3)}
 \item press the start button \textbf(4)
\end{enumerate}

After performing these steps the screen should be filled with results like in
figure \ref{results}. Every result shows the filename and a preview of the
sentence. By left-clicking a result the details will be displayed. The details
contain the full sentence, the edge(s) where the contraint was found in textual
form and an image of the sentence with the edge(s) highlighted. Below the image
there is a link that can be clicked to download the corresponding file in .cda
format. By left-clicking the numbers above the results the displayed page can
be changed.
\begin{figure}
 \frame{\includegraphics[width=1\textwidth]{images/cobacose_results1.png}}
 \caption{THE COBACOSE client after a successful search}
 \label{results}
\end{figure}

The following listing shows the example constraint used in the task which was
described before:
\begin{verbatim}
{X:SYN} : find_negation  : help : 1.0 :
  X@word = nicht |
  X@word = nichts |
  X@word = niemals |
  X@word = nirgends |
  X@word = nie |
  X@word = kaum |
  X@word = keinesfalls |
  X@word = keineswegs |
  X@word = keinerlei |
  X@base = kein |
  X@base = niemand -> false;
\end{verbatim}
The first line is the signature, the rest the formula. The signature is
optional and doesn't need to be provided. The formula describes the properties
of the constraint. In this case different conditions are connected by an or
(\textbf{\textbar}) and each condition states that a certain negation has to be
there. The formulas for the search have to be input negated
(\textbf{-\textgreater false}). 

\subsection{Programmatic Access}
For automated tasks it might be necessary to programmatically access the
servlets that run on the server. There are three web services that can be
accessed: the constraint search servlet (/CSServlet), the image servlet
(/ImageServlet) and the file servlet (/FileServlet).
\newline
A POST request (without arguments) to the /CSServlet will be answered with the
number of sentences the corpus contains. A GET request with the arguments
\textit{from}, \textit{to} and \textit{constraint} will behave the same way as
sending the request through the web interface. The answer will be an array of
JSON objects, where each object represents a result.
\newline
A POST request to the /ImageServlet will be answered by the dimensions of the
images. This is only relevant for the web interface. A GET request with a
\textit{parse} argument will be answered with an image in svg format. The
\textit{parse} argument should contain a single result as a JSON object.
\newline
A GET request with a \textit{file} argument to the /FileServlet will be answered
with a byte stream of the corresponding file, if found.

\end{document}
